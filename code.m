
    




j=2;
Capcty=info{1,4};%Capacity of school bus%

for k=1:nbst%initialiazation of cell array for bus stop student population%
    C{k}=0;
    flagstop{k}=0; %if the stop has been visited by a bus
end
flagstop{1}=1;
for b=1:stpop
    bustopadder{b,1}=0;
    StudentPickedByBus{b,1}=0; %if student was picked up by abus
   
end

for b=1:stpop
    FlagStud{b,1}=0;%1x400 cell array which describes which student went to the bus stop%
    StationStudent{b,1}=b;
    StationStudent{b,2}=-1;
end

for b=1:stpop
    for k=1:nbst
        FlagAlloc{b,k}=0;%cell array that shows evey possible bus stop that a student can go%
    end
end


distance=0;
busadder=0;
for b=1:stpop
    for k=2:nbst
         if(students{b,j}>bus{k,j})
             distance=sqrt((students{b,j}-bus{k,j})^2+(students{b,j+1}-bus{k,j+1})^2);
         elseif(bus{k,j}>students{b,j})
             distance=sqrt((bus{k,j}-students{b,j})^2+(bus{k,j+1}-students{b,j+1})^2);
         elseif(students{b,j}==bus{k,j})
             if(students{b,j+1}>bus{k,j+1})
                 distance=sqrt((students{b,j+1}-bus{k,j+1})^2);
             else
                distance=sqrt((bus{k,j+1}-students{b,j+1})^2);
             end
         end%calculation of eucledian distance between two points%
         if(distance<=info{1,3})%checking if distance is less than max walking distance%
                FlagAlloc{b,k}=1;
                busadder=busadder+1;
         end  
    end
    distance=0;
    bustopadder{b,1}=busadder;%calculating to how many stations each student can go%
    busadder=0;
end
        sortedbustop=sortrows(bustopadder);
        wd=sortedbustop;
        [~,idx]=unique(  strcat(wd(:,1), 'rows'));
        bustopunique=wd(idx,:);
        minsize=size(bustopunique);

        
        for b=1:minsize
            for i=1:stpop
                if(bustopunique{b,1}==bustopadder{i,1})%finds the indexes of the students with the min bus stops%
                   for j=2:nbst
                       if(FlagAlloc{i,j}==1 && FlagStud{i,1}==0)
                           if(C{j}<Capcty)
                               C{j}=C{j}+1;
                               FlagStud{i,1}=1;
                               StationStudent{i,2}=j;
                           end
                           
                               
                       end
          
                  end
                       
                end
            end
        end
        
            
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%STUDENT ALLOCATION TO BUS STOP FINISHED%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
                    
      
      j=2;
      TotalDistanceTravelled=0; %all distance made
      buscounter=0;  
      for b=1:nbst
          for k=1:nbst
              
              if(bus{b,j}>bus{k,j})
                  distance=sqrt((bus{b,j}-bus{k,j})^2+(bus{b,j+1}-bus{k,j+1})^2);
              elseif(bus{k,j}>bus{b,j})
             distance=sqrt((bus{k,j}-bus{b,j})^2+(bus{k,j+1}-bus{b,j+1})^2);
              elseif(bus{b,j}==bus{k,j})
                 if(bus{b,j+1}>bus{k,j+1})
                 distance=sqrt((bus{b,j+1}-bus{k,j+1})^2);
                  else
                distance=sqrt((bus{k,j+1}-bus{b,j+1})^2);
                 end
              end
               DistanceBtwStations{b,k}=distance;    %%%% saves the distances of all bustations with each other and the school
          end
      end
           for k=2:nbst%%%stops that dont have students, are not visited
          if (C{k}==0)
              flagstop{k}=1;
          end
           end
           %GREEDY ALGORITHM
w=1;
       for b=1:nbst
         if(C{b}==Capcty)
             for k=1:stpop
                 if(StationStudent{k,2}==b)
                     StudentPickedByBus{k,1}=1;
                 end                                                                     %%checks if a bus stop is full goes and picks up the students and brings them back to school
             end
                  
            flagstop{b}=1;
            TotalDistanceTravelled=TotalDistanceTravelled + 2*DistanceBtwStations{1,b}; 
           routes{w,1}=b;
           w=w+1;
         end
       end
       
  q=1;     
       
     for b=1:nbst
         
         if(flagstop{b}==0)&&(buscounter+C{b}<=Capcty)       %checks if a bus station has students less than the capacity
            
             buscounter=buscounter+C{b}; 
             for k=1:stpop
                 if(StationStudent{k,2}==b)
                     StudentPickedByBus{k,1}=1;
                 end                                                                     
             end
                laststop=b;                    
                flagstop{b}=1;  
                TotalDistanceTravelled=TotalDistanceTravelled + DistanceBtwStations{1,b};   %picks them up calculates distance traveles to that station
                routes{w,q}=b;
                q=q+1;
                  for p=1:nbst      
                      if(flagstop{p}==0)&&(buscounter+C{p}<=Capcty)         %checks if it can pick up other students to fill the capacity or get as close as possible
                       buscounter=buscounter+C{p};
                          for k=1:stpop
                             if(StationStudent{k,2}==p)
                                  StudentPickedByBus{k,1}=1;
                             end                                                                     
                          end
                          
                          flagstop{p}=1;
                          TotalDistanceTravelled=TotalDistanceTravelled + DistanceBtwStations{p,laststop};    %if yes picks the students and calculates distance from last station to the next
                         routes{w,q}=p;
                         q=q+1;
                          laststop=p;
                      end
                     if (buscounter==Capcty)
                         break
                     end
                  end
                  
                         TotalDistanceTravelled=TotalDistanceTravelled + DistanceBtwStations{1,laststop}; %if cant pick other students goes back to last stop
                         buscounter=0;
                         w=w+1;
               q=1;
            
         end
         
     end

     

      
    
        

     