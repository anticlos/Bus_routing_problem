stpop<-size[students,1]   //student population%
nbst<-size[bus,1]    //number of bus stops%
j=<-2
Capcty<-info[1,4]   //Capacity of school bus%

FOR k FROM 1 UNTIL nbst  DO   //initialiazation of cell array for bus stop student population%
    C{k}<-0
ENDFOR

FOR b FROM 1 UNTIL stpop  DO
    bustopadder[b,1]<-0
ENDFOR

FOR b FROM 1 UNTIL stpop DO
    FlagStud{b,1}=0      //400x1 cell array which describes which student went to the bus stop%
    StationStudent[b,1]<-1
ENDFOR

FOR b FROM 1 UNTIL stpop DO
    FOR k FROM 1 UNTIL nbst  DO
        FlagAlloc[b,k]<-0//cell array that shows evey possible bus stop that a student can go%
    ENDFOR
ENDFOR


distance<-0
busadder<-0
FOR b FROM 1 UNTIL stpop  DO
	FOR k FROM 2 UNTIL nbst  DO
         IF(students[b,j]>bus[k,j])
             distance<-sqrt((students[b,j]-bus[k,j])^2+(students[b,j+1]-bus[k,j+1])^2)
         ELSE_IF(bus[k,j]>students[b,j])
             distance<-sqrt((bus[k,j]-students[b,j])^2+(bus[k,j+1]-students[b,j+1])^2)
         ELSE_IF(students[b,j]=bus[k,j])
             IF(students[b,j+1]>bus[k,j+1])
                 distance<-sqrt((students[b,j+1]-bus[k,j+1])^2)
             ELSE
                distance<-sqrt((bus[k,j+1]-students[b,j+1])^2)
             ENDIF
         ENDIF   //calculation of eucledian distance between two points
         IF(distance<=info[1,3])//checking if distance is less than max walking distance
                FlagAlloc[b,k]<-1
                busadder<-busadder+1
         ENDIF
    ENDFOR
    distance<-0
    bustopadder[b,1]=busadder   //calculating to how many stations each student can go%
    busadder<-0
ENDFOR


sortedbustop<-sortrows[bustopadder)
        wd<-sortedbustop
        [~,idx]<-unique(  strcat(wd(:,1), 'rows'))
        bustopunique<-wd(idx,:);
        minsize<-size(bustopunique)

        
 FOR b FROM 1 UNTIL minsize DO
	FOR I FROM 1 UNTIL stpop DO
                IF(bustopunique[b,1]=bustopadder[i,1])  //finds the indexes of the students with the min bus stops%
                   FOR j FROM 2 UNTIL nbst DO
                       IF(FlagAlloc[i,j]=1 AND FlagStud[i,1]=0)
                           IF(C[j]<Capcty)
                               C[j]<-C[j]+1
                               FlagStud[i,1]<-1
                               StationStudent[i,1]<-j
                           ENDIF
						ENDIF
                           
                               
                    ENDFORFOR
          
                ENDIF
                       
    ENDFOR
ENDFOR




























